package com.glearning.collection;

import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

public class ComparatorDemo {
	
	
	public static void main(String[] args) {
		
		TreeSet<String> elements = new TreeSet<>(Collections.reverseOrder());
		
		elements.add("Arjun");
		elements.add("Ravi");
		elements.add("Kiran");
		elements.add("Raju");
		elements.add("Vinay");
		elements.add("Ramesh");
		elements.add("Chetan");
		elements.add("Suresh");
		
		System.out.println(elements);
		
	}

}

class ReverseOrderComparator implements Comparator<String>{

	@Override
	public int compare(String name1, String name2) {
		return name2.compareToIgnoreCase(name1);
	}
}
