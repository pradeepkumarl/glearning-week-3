package com.glearning.solution;

import java.util.ArrayList;
import java.util.List;

public class LowestCommonAnsestor {
	private static Node root = new Node(10);
	private static List<Integer> path1 = new ArrayList<>();
	private static List<Integer> path2 = new ArrayList<>();
	
	public static void main(String[] args) {
		initializeData();
		// accept two values from the users
		int value1 = 60;
		int value2 = 70;
		Node node1 = new Node(value1);
		Node node2 = new Node(value2);
		populatePathValues(root, node1, node2);
		//find the lowest common ansestor from the path
		int commonAnsestor = findCommonAnsestor(path1, path2);
		System.out.println("Common Ansestor is :: "+ commonAnsestor);
	}
	private static int findCommonAnsestor(List<Integer> path1, List<Integer> path2) {
		System.out.println("Path - 1: "+path1);
		System.out.println("Path - 2: "+path2);
		
		int index = 0;
		for(;index < path1.size(); index ++) {
			if(path1.get(index) != path2.get(index)) {
				break;
			}
		}
		return path1.get(index - 1);
	}
	private static void populatePathValues(Node root, Node node1, Node node2) {
		findPath(root, node1.data, path1 );
		findPath(root, node2.data, path2);
	}
	private static boolean findPath(Node root, int value, List<Integer> path) {
		// the base condition
		if (root == null) {
			return false;
		}
		path.add(root.data);

		// best case scenario
		if (root.data == value) {
			return true;
		}

		if (root.left != null && findPath(root.left, value, path)) {
			return true;
		}
		if (root.right != null && findPath(root.right, value, path)) {
			return true;
		}
		
		path.remove(path.size() - 1);

		return false;
	}

	private static void initializeData() {
		root.left = new Node(20);
		root.right = new Node(30);
		// 2nd level - left side
		root.left.left = new Node(40);
		root.left.right = new Node(50);
		// 2nd level - right side
		root.right.left = new Node(60);
		root.right.right = new Node(70);
	}
}
class Node {
	int data;
	Node left;
	Node right;
	public Node(int data) {
		this.data = data;
		this.left = null;
		this.right = null;
	}
}
