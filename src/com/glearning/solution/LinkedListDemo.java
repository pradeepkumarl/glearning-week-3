package com.glearning.solution;

public class LinkedListDemo {

	public static void main(String[] args) {
		initializaData();
	}

	private static void initializaData() {
		LinkedListNode headNode = new LinkedListNode(10);
		headNode.next = new LinkedListNode(20);
		headNode.next.next = new LinkedListNode(30);
		headNode.next.next.next = new LinkedListNode(40);
		
	}
}

class LinkedListNode {
	private int data;
	LinkedListNode prev;
	LinkedListNode next;
	
	public LinkedListNode(int data) {
		this.data = data;
		this.next = null;
		this.prev = null;
	}
}
