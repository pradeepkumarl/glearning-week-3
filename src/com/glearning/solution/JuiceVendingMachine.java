package com.glearning.solution;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

public class JuiceVendingMachine {

	
	public static void main(String[] args) {
		JuiceVendingMachine vendingMachine = new JuiceVendingMachine();
		// priority queue
		PriorityQueue<Integer> queue = new PriorityQueue<>(Collections.reverseOrder());
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter total number of apple milkshakes");
		int order1 = scanner.nextInt();
		System.out.println("Enter total number of apple milkshakes");
		int order2 = scanner.nextInt();
		System.out.println("Enter total number of apple milkshakes");
		int order3 = scanner.nextInt();
		queue.add(order1);
		queue.add(order2);
		queue.add(order3);
		
		int seconds = vendingMachine.calculateMinTime(queue);
		System.out.println("Total number of seconds to process the orders :: "+ seconds);
		
		scanner.close();

	}

	private int calculateMinTime(PriorityQueue<Integer> queue ) {
		 int seconds = 0;
		 while(!queue.isEmpty()) {
			 //process the orders by priority
			 int value1 = 0;
			 int value2 = 0;
			 //from the top two values, process the orders from the first order
			 if(!queue.isEmpty()) {
				 //process the order;
				 value1 = queue.poll();
			 }
			 //from the top two values, process the orders from the second order
			 if(!queue.isEmpty()) {
				 //process the order;
				 value2 = queue.poll();
			 }
			 //check till one of the value of the variable is 0
			 if(value1 == 0 && value2 > 0) {
				 seconds = seconds + value2;
				 break;
			 }
			 if(value1 > 0 && value2 == 0) {
				 seconds = seconds + value1;
				 break;
			 }
			 if(value1 > 0 && value2 > 0) {
				 value1--;value2--;
				 seconds++;
			 }
			 //if any of the variable is greater than 0, then add that back to the queue
			 if(value1 > 0 ) {
				 queue.add(value1);
			 }
			 if(value2 > 0 ) {
				 queue.add(value2);
			 }
		 }
		 return seconds;
	}
	
	private void printElementsFromQueue(PriorityQueue<Integer> queue ) {
		while (!queue.isEmpty()) {
			System.out.println(queue.poll());
		}
	}

}
